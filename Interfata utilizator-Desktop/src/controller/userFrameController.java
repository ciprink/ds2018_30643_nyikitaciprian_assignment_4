package controller;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import main.services.Package;
import main.services.PackageArray;
import main.services.RouteArray;
import main.services.implementations.*;

public class userFrameController {

        @FXML
        private ResourceBundle resources;

        @FXML
        private URL location;

        @FXML
        private TextField packageNameInput;

        @FXML
        private TextArea textArea;

        @FXML
        private Button myPackagesButton;

        @FXML
        private Button searchPackageButton;

        @FXML
        private Button packageStatusButton;

        private PackageServiceImplService packageService = new PackageServiceImplService();
        private PackageService packagePort = packageService.getPackageServiceImplPort();

        private UserServiceImplService userService = new UserServiceImplService();
        private UserService userPort = userService.getUserServiceImplPort();

        private RouteServiceImplService routeService = new RouteServiceImplService();
        private RouteService routePort = routeService.getRouteServiceImplPort();

        @FXML
        void myPackagesButton(ActionEvent event) {

            PackageArray packs = packagePort.findPackagesBySenderId(7);
            textArea.clear();
            for(int i=0; i< packs.getItem().size(); i++){
                textArea.appendText(
                                packs.getItem().get(i).getId() + " , " +
                                packs.getItem().get(i).getName() + " , " +
                                packs.getItem().get(i).getReceiverCity() + " , " +
                                "\n"
                );
            }
        }

        @FXML
        void packageStatusButtonAction(ActionEvent event) {

            String routeName = packageNameInput.getText();
            RouteArray routes = routePort.findAllRoutesByPackageName(routeName);

            textArea.clear();
            for(int i=0; i< routes.getItem().size(); i++){

                textArea.appendText(
                                        routes.getItem().get(i).getId() + " , " +
                                        routes.getItem().get(i).getPack() + " , " +
                                        routes.getItem().get(i).getCity() + " , " +
                                        routes.getItem().get(i).getTime() + " , " +
                                "\n"
                );
            }

        }

        @FXML
        void searchPackageButtonAction(ActionEvent event) {

            String p = packagePort.findPackage(packageNameInput.getText()).getName();
            Package pack = packagePort.findPackage(p);


            textArea.setText(   "ID:  " +  pack.getId() + "\n" +
                    "NAME:  " +  pack.getName() + "\n" +
                    "DESCRIPTION:  " +  pack.getDescription() + "\n" +
                    "SENDER:  " +  userPort.findUserById(pack.getSenderClient()).getName() + "\n" +
                    "RECEIVER:  " +  userPort.findUserById(pack.getReceiverClient()).getName() + "\n" +
                    "SENDER CITY:  " +  pack.getSenderCity() + "\n" +
                    "RECEIVER CITY:  " +  pack.getReceiverCity());
        }

        @FXML
        void initialize() {
            assert packageNameInput != null : "fx:id=\"packageNameInput\" was not injected: check your FXML file 'userFrameController.fxml'.";
            assert myPackagesButton != null : "fx:id=\"myPackagesButton\" was not injected: check your FXML file 'userFrameController.fxml'.";
            assert searchPackageButton != null : "fx:id=\"searchPackageButton\" was not injected: check your FXML file 'userFrameController.fxml'.";
            assert packageStatusButton != null : "fx:id=\"packageStatusButton\" was not injected: check your FXML file 'userFrameController.fxml'.";
            assert textArea != null : "fx:id=\"textArea\" was not injected: check your FXML file 'userFrame.fxml'.";


        }
}

