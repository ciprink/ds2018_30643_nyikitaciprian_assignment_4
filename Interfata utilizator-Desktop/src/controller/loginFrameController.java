package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.services.User;
import main.services.implementations.UserService;
import main.services.implementations.UserServiceImplService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class loginFrameController{

        @FXML
        private ResourceBundle resources;

        @FXML
        private URL location;

        @FXML
        private Label titleLabel;

        @FXML
        private Label usernameLabel;

        @FXML
        private Label passwordLabel;

        @FXML
        private TextField usernameInput;

        @FXML
        private Button loginButton;

        @FXML
        private Button registerButton;

        @FXML
        private PasswordField passwordInput;


        //==============================================================================================================

        private UserServiceImplService userService = new UserServiceImplService();
        private UserService userPort = userService.getUserServiceImplPort();

        @FXML
        void loginButtonAction(ActionEvent event) {

             String username = usernameInput.getText();
             String pass = passwordInput.getText();
             User checkUser = userPort.findUserByName(username);

             if(checkUser.getRole().equals("admin")){

                 if(checkUser.getPassword().equals(pass)) {

                     try {
                         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/adminFrame.fxml"));
                         Parent root1 = (Parent) fxmlLoader.load();
                         Stage stage = new Stage();
                         stage.setScene(new Scene(root1));
                         stage.show();
                     } catch (IOException e) {
                         System.err.println(e.getMessage());
                     }
                 }
                 else
                 {
                     System.out.println("Wrong password");
                 }
             }
             else
             {
                 try {
                     FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/userFrame.fxml"));
                     Parent root1 = (Parent) fxmlLoader.load();
                     Stage stage = new Stage();
                     stage.setScene(new Scene(root1));
                     stage.show();
                 } catch (IOException e) {
                     System.err.println(e.getMessage());
                 }
             }
        }

        @FXML
        void registerButtonAction(ActionEvent event) {

            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/registerFrame.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root1));
                stage.show();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }

        }

        @FXML
        void initialize() {
            assert titleLabel != null : "fx:id=\"titleLabel\" was not injected: check your FXML file 'Untitled'.";
            assert usernameLabel != null : "fx:id=\"usernameLabel\" was not injected: check your FXML file 'Untitled'.";
            assert passwordLabel != null : "fx:id=\"passwordLabel\" was not injected: check your FXML file 'Untitled'.";
            assert usernameInput != null : "fx:id=\"usernameInput\" was not injected: check your FXML file 'Untitled'.";
            assert passwordInput != null : "fx:id=\"passwordInput\" was not injected: check your FXML file 'Untitled'.";
            assert loginButton != null : "fx:id=\"loginButton\" was not injected: check your FXML file 'Untitled'.";
            assert registerButton != null : "fx:id=\"registerButton\" was not injected: check your FXML file 'Untitled'.";


        }
    }

