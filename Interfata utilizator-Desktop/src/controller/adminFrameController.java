package controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import main.services.Package;
import main.services.Route;
import main.services.User;
import main.services.implementations.*;

public class adminFrameController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button addPackageButton;

    @FXML
    private Button deletePackageButton;

    @FXML
    private TextField idInput;

    @FXML
    private TextField nameInput;

    @FXML
    private TextField descriptionInput;

    @FXML
    private TextField senderInput;

    @FXML
    private TextField receiverInput;

    @FXML
    private TextField senderCityInput;

    @FXML
    private TextField receiverCityInput;

    @FXML
    private Label idLabel;

    @FXML
    private Label nameLabel;

    @FXML
    private Label descriptionLabel;

    @FXML
    private Label senderLabel;

    @FXML
    private Label receiverLabel;

    @FXML
    private Label senderCityLabel;

    @FXML
    private Label receiverCityLabel;

    @FXML
    private TextField packageNameInput;

    @FXML
    private TextField cityInput;

    @FXML
    private TextField timeInput;

    @FXML
    private Label packageNameLabel;

    @FXML
    private Label cityLabel;

    @FXML
    private Label timeLabel;

    @FXML
    private Button addRouteButton;

    @FXML
    private Button findPackageButton;

    @FXML
    private TextArea textArea;

    //==================================================================================================================

    private PackageServiceImplService packageService = new PackageServiceImplService();
    private PackageService packagePort = packageService.getPackageServiceImplPort();

    private UserServiceImplService userService = new UserServiceImplService();
    private UserService userPort = userService.getUserServiceImplPort();

    private RouteServiceImplService routeService = new RouteServiceImplService();
    private RouteService routePort = routeService.getRouteServiceImplPort();

    private int getIdInput() { return Integer.parseInt(idInput.getText()); }
    private String getNameInput() { return nameInput.getText(); }
    private String getDescriptionInput() { return descriptionInput.getText(); }
    private String getSenderInput() { return senderInput.getText(); }
    private String getReceiverInput() { return receiverInput.getText(); }
    private String getSenderCityInput() { return senderCityInput.getText(); }
    private String getReceiverCityInput() { return receiverCityInput.getText(); }

    @FXML
    void addPackageButtonAction(ActionEvent event) {

        if( idInput.getText().equals("") )
        {
            System.out.println("id null");
        }
        else {
            int id = getIdInput();
        }
        String name = getNameInput();
        String description = getDescriptionInput();
        String sender = getSenderInput();
        String receiver = getReceiverInput();
        String senderCity = getSenderCityInput();
        String receiverCity = getReceiverCityInput();

        User senderUser = userPort.findUserByName(sender);
        User receiverUser = userPort.findUserByName(receiver);

        int senderId = senderUser.getId();
        int receiverId = receiverUser.getId();

        Package pack = new Package();

        pack.setName(name);
        pack.setDescription(description);
        pack.setSenderClient(senderId);
        pack.setReceiverClient(receiverId);
        pack.setSenderCity(senderCity);
        pack.setReceiverCity(receiverCity);
        pack.setTracking(false);

        packagePort.addPackage(pack);
        System.out.println("Package added.");
    }

    @FXML
    void addRouteButtonAction(ActionEvent event) {

        String packName = packageNameInput.getText();
        String city = cityInput.getText();
        String time = timeInput.getText();

        Route route = new Route();
        route.setPack(packName);
        route.setCity(city);
        route.setTime(time);

        routePort.addRoute(route);
    }

    @FXML
    void deletePackageButtonAction(ActionEvent event) {

        String name = getNameInput();
        Package pack = packagePort.findPackage(name);
        packagePort.deletePackage(pack);
        System.out.println("Package deleted.");
    }

    @FXML
    void findPackageButtonAction(ActionEvent event) {

        String packageName = getNameInput();
        Package pack = packagePort.findPackage(packageName);

        textArea.setText(   "ID:  " +  pack.getId() + "\n" +
                          "NAME:  " +  pack.getName() + "\n" +
                   "DESCRIPTION:  " +  pack.getDescription() + "\n" +
                        "SENDER:  " +  userPort.findUserById(pack.getSenderClient()).getName() + "\n" +
                      "RECEIVER:  " +  userPort.findUserById(pack.getReceiverClient()).getName() + "\n" +
                   "SENDER CITY:  " +  pack.getSenderCity() + "\n" +
                 "RECEIVER CITY:  " +  pack.getReceiverCity());

    }

    @FXML
    void initialize() {
        assert addPackageButton != null : "fx:id=\"addPackageButton\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert deletePackageButton != null : "fx:id=\"deletePackageButton\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert idInput != null : "fx:id=\"idInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert nameInput != null : "fx:id=\"nameInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert descriptionInput != null : "fx:id=\"descriptionInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert senderInput != null : "fx:id=\"senderInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert receiverInput != null : "fx:id=\"receiverInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert senderCityInput != null : "fx:id=\"senderCityInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert receiverCityInput != null : "fx:id=\"receiverCityInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert idLabel != null : "fx:id=\"idLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert nameLabel != null : "fx:id=\"nameLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert descriptionLabel != null : "fx:id=\"descriptionLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert senderLabel != null : "fx:id=\"senderLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert receiverLabel != null : "fx:id=\"receiverLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert senderCityLabel != null : "fx:id=\"senderCityLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert receiverCityLabel != null : "fx:id=\"receiverCityLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert packageNameInput != null : "fx:id=\"packageNameInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert cityInput != null : "fx:id=\"cityInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert timeInput != null : "fx:id=\"timeInput\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert packageNameLabel != null : "fx:id=\"packageNameLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert cityLabel != null : "fx:id=\"cityLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert timeLabel != null : "fx:id=\"timeLabel\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert addRouteButton != null : "fx:id=\"addRouteButton\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert findPackageButton != null : "fx:id=\"findPackageButton\" was not injected: check your FXML file 'adminFrame.fxml'.";
        assert textArea != null : "fx:id=\"textArea\" was not injected: check your FXML file 'adminFrame.fxml'.";

    }
}
