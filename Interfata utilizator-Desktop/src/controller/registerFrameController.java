package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.services.User;
import main.services.implementations.UserService;
import main.services.implementations.UserServiceImplService;

public class registerFrameController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField usernameInput;

    @FXML
    private Label titleLabel;

    @FXML
    private Label usernameLabel;

    @FXML
    private Label passwordLabel;

    @FXML
    private Label confirmPasswordLabel;

    @FXML
    private Button registerButton;

    @FXML
    private Label matchPasswordLabel;

    @FXML
    private PasswordField passwordInput;


    private UserServiceImplService userService = new UserServiceImplService();
    private UserService userPort = userService.getUserServiceImplPort();

    @FXML
    void registerButtonAction(ActionEvent event) {
        String username = usernameInput.getText();
        String pass = passwordInput.getText();

        User newUser = new User();
        newUser.setName(username);
        newUser.setPassword(pass);
        newUser.setRole("user");

        userPort.addUser(newUser);

        Stage stage = (Stage) registerButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    void initialize() {
        assert usernameInput != null : "fx:id=\"usernameInput\" was not injected: check your FXML file 'registerFrame.fxml'.";
        assert titleLabel != null : "fx:id=\"titleLabel\" was not injected: check your FXML file 'registerFrame.fxml'.";
        assert usernameLabel != null : "fx:id=\"usernameLabel\" was not injected: check your FXML file 'registerFrame.fxml'.";
        assert passwordLabel != null : "fx:id=\"passwordLabel\" was not injected: check your FXML file 'registerFrame.fxml'.";
        assert confirmPasswordLabel != null : "fx:id=\"confirmPasswordLabel\" was not injected: check your FXML file 'registerFrame.fxml'.";
        assert registerButton != null : "fx:id=\"registerButton\" was not injected: check your FXML file 'registerFrame.fxml'.";
        assert matchPasswordLabel != null : "fx:id=\"matchPasswordLabel\" was not injected: check your FXML file 'registerFrame.fxml'.";
        assert passwordInput != null : "fx:id=\"passwordInput\" was not injected: check your FXML file 'registerFrame.fxml'.";

    }
}
