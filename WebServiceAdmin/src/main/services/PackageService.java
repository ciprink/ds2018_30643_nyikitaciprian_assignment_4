package main.services;

import main.entities.Package;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface PackageService {

    @WebMethod
    public void addPackage(Package pack);
    @WebMethod
    public Package findPackageById(int id);
    @WebMethod
    public void deletePackage(Package pack);

}
