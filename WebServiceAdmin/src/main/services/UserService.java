package main.services;

import main.entities.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserService {

    @WebMethod
    public void addUser(User user);

    @WebMethod
    public User findUserById(int id);

    @WebMethod
    public User findUserByName(String name);

    @WebMethod
    public void deleteUser(User user);
}
