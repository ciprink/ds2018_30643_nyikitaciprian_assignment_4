package main.services;

import main.entities.Route;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface RouteService {

    @WebMethod
    public void updateStatusRoute(Route route);
    @WebMethod
    public Route findRouteById(int id);
    @WebMethod
    public Route findRouteByPackageId(int package_id);
    @WebMethod
    public Route[] findAllRoutesByPackageId(int package_id);
}
