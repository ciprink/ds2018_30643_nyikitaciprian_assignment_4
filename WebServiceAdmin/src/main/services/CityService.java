package main.services;

import main.entities.City;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface CityService {

    @WebMethod
    public City findCityById(int id);
    @WebMethod
    public City findCityByName(String name);
    @WebMethod
    public City[] findAllCities();
}
