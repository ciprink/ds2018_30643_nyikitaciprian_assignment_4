package main.services.implementations;

import main.dao.RouteDAO;
import main.entities.Route;
import main.services.RouteService;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "main.services.RouteService")
public class RouteServiceImpl implements RouteService {

    RouteDAO routeDAO = new RouteDAO();

    @Override
    public void updateStatusRoute(Route route) {
        routeDAO.updateStatusRoute(route);
    }

    @Override
    public Route findRouteById(int id) {

        return routeDAO.findRouteById(id);
    }

    @Override
    public Route findRouteByPackageId(int package_id) {

        return routeDAO.findRouteByPackageId(package_id);
    }

    @Override
    public Route[] findAllRoutesByPackageId(int package_id) {

        ArrayList<Route> list = (ArrayList<Route>) routeDAO.findAllRoutesByPackageId(package_id);

        Route[] routeArray = new Route[list.size()];

        for(int i=0; i< list.size();i++) {
            routeArray[i] = list.get(i);
        }

        return routeArray;
    }

}
