package main.services.implementations;

import main.dao.CityDAO;
import main.entities.City;
import main.entities.Route;
import main.services.CityService;

import javax.jws.WebService;
import java.util.ArrayList;

@WebService(endpointInterface = "main.services.CityService")
public class CityServiceImpl implements CityService {

    CityDAO cityDAO = new CityDAO();

    @Override
    public City findCityById(int id) {
        return cityDAO.findCityById(id);
    }

    @Override
    public City findCityByName(String name) {
        return cityDAO.findCityByName(name);
    }

    @Override
    public City[] findAllCities() {

        ArrayList<City> list = (ArrayList<City>) cityDAO.findAllCities();
        City[] cityArray = new City[list.size()];

        for(int i=0; i< list.size();i++) {
            cityArray[i] = list.get(i);
        }

        return cityArray;
    }
}
