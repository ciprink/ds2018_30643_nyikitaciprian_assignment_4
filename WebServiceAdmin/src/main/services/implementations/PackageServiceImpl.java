package main.services.implementations;

import main.dao.PackageDAO;
import main.entities.Package;
import main.services.PackageService;

import javax.jws.WebService;

@WebService(endpointInterface = "main.services.PackageService")
public class PackageServiceImpl implements PackageService {

    PackageDAO packageDAO = new PackageDAO();

    @Override
    public void addPackage(Package pack) {
        packageDAO.addPackage(pack);
    }

    @Override
    public Package findPackageById(int id) {
        return packageDAO.findPackage(id);
    }

    @Override
    public void deletePackage(Package pack) {
        packageDAO.deletePackage(pack);
    }
}
