package main.services.implementations;

import main.dao.UserDAO;
import main.entities.User;
import main.services.UserService;

import javax.jws.WebService;

@WebService(endpointInterface = "main.services.UserService")
public class UserServiceImpl implements UserService {

    UserDAO userDAO = new UserDAO();

    @Override
    public void addUser(User user) {
        userDAO.addUser(user);
    }

    @Override
    public User findUserById(int id) {
        return userDAO.findUserById(id);
    }

    @Override
    public User findUserByName(String name) {
        return userDAO.findUserByName(name);
    }

    @Override
    public void deleteUser(User user) {
        userDAO.deleteUser(user);
    }
}
