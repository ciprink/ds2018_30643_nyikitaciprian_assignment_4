package main.entities;

public class Package {


    private int id;
    private String name;
    private String description;
    private Boolean tracking;
    private User senderClient;
    private User receiverClient;
    private City senderCity;
    private City receiverCity;

    public Package(int id, String name, String description, User senderClient, User receiverClient, City senderCity, City receiverCity) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.tracking = false;
        this.senderClient = senderClient;
        this.receiverClient = receiverClient;
        this.senderCity = senderCity;
        this.receiverCity = receiverCity;
    }

    public Package(String name, String description, User senderClient, User receiverClient, City senderCity, City receiverCity) {

        this.name = name;
        this.description = description;
        this.tracking = false;
        this.senderClient = senderClient;
        this.receiverClient = receiverClient;
        this.senderCity = senderCity;
        this.receiverCity = receiverCity;
    }

    public Package() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getTracking() {
        return tracking;
    }

    public void setTracking(Boolean tracking) {
        this.tracking = tracking;
    }

    public User getSenderClient() {
        return senderClient;
    }

    public void setSenderClient(User senderClient) {
        this.senderClient = senderClient;
    }

    public User getReceiverClient() {
        return receiverClient;
    }

    public void setReceiverClient(User receiverClient) {
        this.receiverClient = receiverClient;
    }

    public City getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(City senderCity) {
        this.senderCity = senderCity;
    }

    public City getReceiverCity() {
        return receiverCity;
    }

    public void setReceiverCity(City receiverCity) {
        this.receiverCity = receiverCity;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", tracking=" + tracking +
                ", senderClient=" + senderClient +
                ", receiverClient=" + receiverClient +
                ", senderCity=" + senderCity +
                ", receiverCity=" + receiverCity +
                '}';
    }
}
