package main.entities;

public class Route {

    private int id;
    private String city;
    private String time;
    private Package pack;

    public Route(int id, String city, String time, Package pack) {
        this.id = id;
        this.city = city;
        this.time = time;
        this.pack = pack;
    }

    public Route(String city, String time, Package pack) {
        this.city = city;
        this.time = time;
        this.pack = pack;
    }

    public Route() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Package getPack() {
        return pack;
    }

    public void setPack(Package pack) {
        this.pack = pack;
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", time='" + time + '\'' +
                ", packageId=" + pack +
                '}';
    }
}
