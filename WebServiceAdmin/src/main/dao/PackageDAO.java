package main.dao;

import main.entities.Package;
import main.entities.Route;
import main.services.RouteService;
import main.services.implementations.RouteServiceImpl;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class PackageDAO {

    public void addPackage(Package pack) {
        int packageId = -1;
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            packageId = (Integer) session.save(pack);
            pack.setId(packageId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Package findPackage(int id) {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        List<Package> packs = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE id = :id");
            query.setParameter("id", id);
            packs = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return packs != null && !packs.isEmpty() ? packs.get(0) : null;
    }

    public List<Package> findPackages() {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        List<Package> packs = null;
        try {
            tx = session.beginTransaction();
            packs = session.createQuery("FROM Package").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return packs;
    }

    public static void editPackage(Package p) {

        try(
                Session session = new Configuration().configure().buildSessionFactory().openSession())

        {
            List<Package> packages = session.createQuery("from Package where name = \'" + p.getName() + "\'", Package.class).list();
            Package[] packagesArray = new Package[packages.size()];
            packagesArray = packages.toArray(packagesArray);

            Transaction tx = null;
            tx = session.beginTransaction();

            if (packagesArray.length == 1) {


                Package package1 = packagesArray[0];

                package1.setName(p.getName());
                package1.setTracking(p.getTracking());
                package1.setDescription(p.getDescription());
                package1.setReceiverCity(p.getReceiverCity());
                package1.setSenderCity(p.getSenderCity());
                package1.setReceiverClient(p.getReceiverClient());
                package1.setSenderClient(p.getSenderClient());

                System.out.println("++++++++++++++++++++++++" + package1.toString());
            }

            session.update(p);
            tx.commit();
        }
    }

    public void updatePackage(Package p) {
        try (Session session = new Configuration().configure().buildSessionFactory().openSession()) {

            List<Package> packages = session.createQuery("from Package where name = \'" + p.getName()+ "\'", Package.class).list();
            Package[] packagesArray = new Package[packages.size()];
            packagesArray = packages.toArray(packagesArray);

            if (packagesArray.length == 1) {

                session.beginTransaction();
                Package package1 = packagesArray[0];

                package1.setName(p.getName());
                package1.setTracking(p.getTracking());
                package1.setDescription(p.getDescription());
                package1.setReceiverCity(p.getReceiverCity());
                package1.setSenderCity(p.getSenderCity());
                package1.setReceiverClient(p.getReceiverClient());
                package1.setSenderClient(p.getSenderClient());

                session.update(package1);
                session.evict(package1);
                session.getTransaction().commit();

                System.out.println("+++++++++++++++++++++++++++++++++" + package1.toString());
            }


        }
    }

    public void deletePackage(Package pack) {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(pack);
            session.evict(pack);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void trackPackage(Package pack, Route route){

        pack.setTracking(true);
        updatePackage(pack);

        RouteService  routeService = new RouteServiceImpl();
        routeService.updateStatusRoute(route);
    }


}
