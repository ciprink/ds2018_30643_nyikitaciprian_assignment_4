package main.dao;

import main.entities.Route;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.Array;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class RouteDAO {

    public void updateStatusRoute(Route route) {
        int routeId = -1;
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            routeId = (Integer) session.save(route);
            route.setId(routeId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Route findRouteById(int id) {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        List<Route> routes = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Route WHERE id = :id");
            query.setParameter("id", id);
            routes = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return routes != null && !routes.isEmpty() ? routes.get(0) : null;
    }

    public Route findRouteByPackageId(int package_id) {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        List<Route> routes = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Route WHERE pack = :package_id");
            query.setParameter("package_id", package_id);
            routes = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return routes != null && !routes.isEmpty() ? routes.get(0) : null;
    }

    public List<Route> findAllRoutesByPackageId(int package_id) {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        List<Route> routes=null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Route WHERE pack = :package_id");
            query.setParameter("package_id", package_id);

            routes = query.list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return routes != null && !routes.isEmpty() ? routes : null;
    }
}
