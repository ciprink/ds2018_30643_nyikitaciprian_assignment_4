package main.publisher;
import com.sun.prism.shader.Mask_TextureSuper_AlphaTest_Loader;
import main.dao.PackageDAO;
import main.entities.City;
import main.entities.Package;
import main.entities.Route;
import main.entities.User;
import main.services.CityService;
import main.services.PackageService;
import main.services.RouteService;
import main.services.UserService;
import main.services.implementations.CityServiceImpl;
import main.services.implementations.PackageServiceImpl;
import main.services.implementations.RouteServiceImpl;
import main.services.implementations.UserServiceImpl;

import javax.xml.ws.Endpoint;
import java.util.List;

public class PublisherService {

    public static void main(String[] args) {

        // wsimport -s . http://localhost:8081/package?wsdl
        Endpoint.publish("http://localhost:8081/package", new PackageServiceImpl());

        // wsimport -s . http://localhost:8081/route?wsdl
        Endpoint.publish("http://localhost:8081/route", new RouteServiceImpl());

        // wsimport -s . http://localhost:8081/city?wsdl
        Endpoint.publish("http://localhost:8081/city", new CityServiceImpl());

        // wsimport -s . http://localhost:8081/user?wsdl
        Endpoint.publish("http://localhost:8081/user", new UserServiceImpl());

        System.out.println("Service running...");


// =====================================================================================================================



    }
}
